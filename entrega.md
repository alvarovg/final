# ENTREGA CONVOCATORIA ENERO
Nombre: Álvaro Vegas González, Correo: a.vegasg.2023@alumnos.urjc.es

Video: https://drive.google.com/file/d/1uUZeyHy5NEgzWxB8jbvGi3QPKDH4wY8G/view?usp=sharing

Requisitos mínimos:
    Método change_colors,
    Método rotate_right,
    Método rotate_colors,
    Método blur,
    Método shift,
    Método crop,
    Método greyscale,
    Método filter,
    Programa transform_simple.py,
    Programa trasnsform_args.py,
    Programa transform_multi.py  

Requisitos opcionales propios:
    Programa transform_menu.py
