import sys
import images
from transforms import rotate_right, mirror, blur, grayscale

def main():
    archivo = sys.argv[1]
    transformacion = sys.argv[2]

    if "/" in archivo:
        i = archivo.split("/")
        nombre, formato = i[-1].split(".")

    else:
        nombre, formato = archivo.split(".")


    if transformacion in ["rotate_right", "mirror", "blur", "grayscale"]:
        imagen = images.read_img(archivo)

        if transformacion == "rotate_right":
            m = rotate_right(imagen)
        elif transformacion == "mirror":
            m = mirror(imagen)
        elif transformacion == "blur":
            m = blur(imagen)
        elif transformacion == "grayscale":
            m =grayscale(imagen)

        images.write_img(m, f"../{nombre}_trans.{formato}" )

if __name__ == '__main__':
    main()