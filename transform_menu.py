import transforms
import  images

def menu():

    while True:
        print("\nCopia la ruta de el archivo de imagen que deseas modificar:")
        ruta = input()
        try:
            imagen = images.read_img(ruta)
            break
        except:
            print("Esta ruta no es válida")

    if "/" in ruta:
        i = ruta.split("/")
        nombre, formato = i[-1].split(".")
    else:
        nombre, formato = ruta.split(".")

    seguir = True
    aplicados = ["", "", "", "", "", "", "", "", "", ""]
    while seguir:

        print("-"* 47,
              "\n"+" ".join(list("MENU DE TRANSFORMACIONES")),
              "\n"+ "-"* 47,
              "\n1.Rotar a la derecha"+ aplicados[1],
              "\n2.Cambiar colores"+ aplicados[2],
              "\n3.Rotar colores"+ aplicados[3],
              "\n4.Espejo"+ aplicados[4],
              "\n5.Blur"+ aplicados[5],
              "\n6.Shift"+ aplicados[6],
              "\n7.Crop"+ aplicados[7],
              "\n8.Escala de grises"+ aplicados[8],
              "\n9.Filtro\n"+ aplicados[9],
              "\n0.APLICAR Y SALIR"
              "\nX.CANCELAR")
        print("-"*47)
        transformacion = input("Elige el número de la transformacion: ")
        print("\n")

        if transformacion == "1":
            imagen = transforms.rotate_right(imagen)
            aplicados[1] = "  (aplicado)"

        elif transformacion == "2":
            print("Escribe en RGB el color que quieres cambiar")
            i = input()
            color_i = (i.split(" "))

            print("Escribe en RGB el color al que quieres cambiar")
            f = input()
            color_f = (f.split(" "))

            for n in range(0,3):
                color_i[n] = int(color_i[n])
                color_f[n] = int(color_f[n])
            color_i = tuple(color_i)
            color_f = tuple(color_f)

            imagen = transforms.change_colors(imagen, color_i,color_f)
            aplicados[2] = "  (aplicado)"

        elif transformacion == "3":
            print("Escribe que número quieres aumentar o disminuir el color de los pixeles")
            cambio = int(input())
            imagen = transforms.rotate_colors(imagen, cambio)
            aplicados[3] = "  (aplicado)"

        elif transformacion == "4":
            imagen = transforms.mirror(imagen)
            aplicados[4] = "  (aplicado)"

        elif transformacion == "5":
            imagen = transforms.blur(imagen)
            aplicados[5] = "  (aplicado)"

        elif transformacion == "6":
            print("Escribe cuantos pixeles quieres mover hacia arriba o abajo la imagen")
            mov_v = int(input())
            print("Escribe cuantos pixeles quieres mover la derecha o izquierda la imagen")
            mov_h = int(input())

            imagen = transforms.shift(imagen, mov_h, mov_v)
            aplicados[6] = "  (aplicado)"

        elif transformacion == "7":
            print("Escribe las cordenadas del pixel que quires que sea la esquina superior izquierda de la imagen")
            x = int(input("x: "))
            y = int(input("y: "))

            print("Escribe de cuántos píxeles quieres que sea el alto y el ancho de la imagen:")
            alto = int(input("Alto: "))
            ancho = int(input("Ancho: "))

            imagen = transforms.crop(imagen, x, y, ancho, alto)
            aplicados[7] = "  (aplicado)"

        elif transformacion == "8":
            imagen = transforms.grayscale(imagen)
            aplicados[8] = "  (aplicado)"

        elif transformacion == "9":
            print("Escribe por cuanto quieres multiplicar cada valor de RGB de los pixeles de la imagen")
            r = float(input("R: "))
            g = float(input("G: "))
            b = float(input("B: "))
            imagen = transforms.filter(imagen, r, g, b)
            aplicados[9] = "  (aplicado)"
        elif transformacion == "0":
            images.write_img(imagen, f"../{nombre}_trans.{formato}")
            print(f"Su imagen ha sido transformada y guardada coorectamente con el nombre: {nombre}_trans.{formato}")
            seguir = False
        elif transformacion == "X" or transformacion == "x":
            seguir = False
        else:
            print("Esa transformacion no existe, intente de nuevo")
def main():
    menu()

main()