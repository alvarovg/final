
import sys
import images
import transforms

def main():
    archivo = sys.argv[1]
    transformacion = sys.argv[2]

    if "/" in archivo:
        i = archivo.split("/")
        nombre, formato = i[-1].split(".")

    else:
        nombre, formato = archivo.split(".")

    if transformacion in ["rotate_right", "mirror", "blur", "grayscale", "crop","shift", "change_colors", "filter", "rotate_colors"]:
        imagen = images.read_img(archivo)

        if transformacion == "rotate_right":
            m = transforms.rotate_right(imagen)
        elif transformacion == "mirror":
            m = transforms.mirror(imagen)
        elif transformacion == "blur":
            m = transforms.blur(imagen)
        elif transformacion == "grayscale":
            m = transforms.grayscale(imagen)
        elif transformacion == "crop":
            m = transforms.crop(imagen, int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), int(sys.argv[6]))
        elif transformacion == "shift":
            m = transforms.shift(imagen, int(sys.argv[3]), int(sys.argv[4]))
        elif transformacion == "change_colors":
            m = transforms.change_colors(imagen, (int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5])),
                                         (int(sys.argv[6]), int(sys.argv[7]), int(sys.argv[8])))
        elif transformacion == "filter":
            m = transforms.filter(imagen, float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]))
        elif transformacion == "rotate_colors":
            m = transforms.rotate_colors(imagen, int(sys.argv[3]))

        images.write_img(m, f"../{nombre}_trans.{formato}" )

if __name__ == '__main__':
    main()